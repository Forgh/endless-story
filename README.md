# endless-story

Hands-on Git workshop to learn about Git clone, rebase and merge

## Cloning a repo 

`git clone git@gitlab.eur.zimmer.com:campmasm/endless-story.git`

OR 

`git clone http://gitlab.eur.zimmer.com/campmasm/endless-story.git`

## Check-out an existing branch 

To check-out develop:

`git checkout develop`

## Create a new branch

`git checkout -b feature/myTask`

## Track a new file

`git add myfilename`

## Commit tracked files
`git commit -am "[My Task ID] My task description"`

## Push my branch

If it is the first time you are pushing this branch :

`git push -u origin feature/myTask`

Else :

`git push`


## Update my feature branch on develop

### Via rebase

`git pull --rebase origin develop`

### Via merge

`git pull --merge origin develop`


## Squashing


After a first commit, you might want to have a single commit in your branch.

### If you only committed once in this branch
You can meld your tracked / added files (git add ...) into the previous commit

`git commit --amend` 


### If you committed multiple times already

`git rebase -i HEAD~x`

Where x is the number of commits you want to squash together, starting from the last commit.

### Push your branch after an amend / squash

`git push --force-with-lease`